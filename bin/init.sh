#!/usr/bin/env bash

if [ ! -d ./tmp ]; then
    mkdir ./tmp
fi
cd ./tmp
if [ ! -f words2.txt ]; then
    wget https://raw.githubusercontent.com/docdis/english-words/master/words2.txt
fi
# Оставить только слова длиннее 5 символов
awk 'length($0) > 5' words2.txt > words.txt
cd ..

# Только создать таблицу
python manage.py migrate contenttypes
python manage.py migrate app 0001

# Загрузить данные
date
echo "Generating data"
time python manage.py generate_data

date
echo "Importing DB"
for (( N = 0; N < 10; N++ ))
do
    echo "\\copy app_record_big FROM './tmp/db-${N}.csv' DELIMITER ',' CSV;" | psql -Ufastdb fastdb;
    date
    echo "Imported part #${N}"
done


# Импорт данных напрямую в партиционированные таблицы
#
# date
# echo "Importing DB"
# for (( N = 0; N < 10; N++ ))
# do
#     N1=$(($N + 1))
#     echo "\\copy app_record_`printf "%02d" ${N1}` FROM './tmp/db-${N}.csv' DELIMITER ',' CSV;" | psql -Ufastdb fastdb;
#     date
#     echo "Imported part #${N}"
# done


# rm ./tmp/db*.csv

# Построить индексы
echo "Creating indexes"
date
python manage.py migrate app
echo "Indexes created"
date
