#!/usr/bin/env bash

./manage.py generate_urls --total 500000
mv ./tmp/urls.txt ./tmp/urls-all.txt
/manage.py generate_urls --records --total 500000
cat ./tmp/urls.txt >> ./tmp/urls-all.txt
siege -c1 -b -i -f ./tmp/urls-all.txt


