import logging
import random
import time

from django.conf import settings
from django.db import connection, models, transaction
from django.utils.functional import cached_property


if settings.MODE == settings.M_PARTITIONED:
    db_table_suffix = ''
else:
    db_table_suffix = '_big'

cursor = connection.cursor()
random.seed()

logger = logging.getLogger('django')

RANDOM_CHOICES = range(1, settings.MAX_RECORDS)


class Record(models.Model):
    RANDOM_SQL = """
        SELECT "number", "title" FROM {db_table}
        WHERE "number" IN (
            SELECT round(random() * {max_records})::integer AS num
            FROM generate_series(1, {max_series})
            GROUP BY num   -- Убрать повторы
        )
        LIMIT {quantity};
    """

    RANDOM_SQL_VALUES = """
        SELECT "number", "title" FROM {db_table}
        WHERE "number" IN ({values})
        LIMIT {quantity};
    """

    # Помечено как primary key, чтобы Django ORM не обращался к полю `id`
    number = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=30)
    text = models.TextField()

    class Meta:
        managed = False
        ordering = ['number']
        db_table = 'app_record%s' % db_table_suffix

    def __str__(self):
        return str(self.number)

    @classmethod
    def _get_random_numbers(cls, quantity=3, fast=False):
        db_table = cls._meta.db_table

        random_sql = cls.RANDOM_SQL.format(
            max_records=settings.MAX_RECORDS,
            max_series=settings.MAX_SERIES,
            quantity=quantity,
            db_table=db_table,
        )
        # values = map(str, random.sample(RANDOM_CHOICES, settings.MAX_SERIES))
        # random_sql = cls.RANDOM_SQL_VALUES.format(
        #     values=','.join(values),
        #     quantity=quantity,
        #     db_table=db_table,
        # )
        if fast:
            random_sql = random_sql.replace(
                'SELECT "number", "title" FROM',
                'SELECT "number" FROM'
            )
        # logger.info('\tExecuting SQL, fetching records')
        cursor.execute(random_sql)
        # logger.info('Query: {}'.format(random_sql))
        records = list(cursor.fetchall())
        # logger.info('\tRecords fetched')
        # logger.info('Total (random internal): %.3f' % (time.time() - st))
        return records

    @classmethod
    def get_random_numbers(cls, quantity=3, fast=False):
        st = time.time()
        # logger.info('Generating random numbers')
        i = settings.MAX_RANDOM_RETRIES
        numbers = []
        while len(numbers) < quantity and i > 0:
            numbers.extend(
                cls._get_random_numbers(
                    quantity=quantity - len(numbers), fast=fast,
                )
            )
            i -= 1
        numbers = set(numbers)
        # logger.info('Total (random numbers): %.3f' % (time.time() - st))
        return numbers

    @cached_property
    def _groups(self):
        return list(Group.objects.values_list('group', 'number'))

    # @classmethod
    def get_random_numbers_fast(self, quantity=3):
        """
        Быстрый выбор случайных.
        Сгенерировать случайные смещения относительно записей в таблице Group
        Выбрать записи с такими смещениями.
        Поскольку для последней группы возможно по смещению число не будет
        найдено, то выполнить с запасом - на 1 раз больше.
        Количество групп небольшое, поэтому можно использовать COUNT(*),
        при увеличении можно заменить.
        Данный метод дает однозначное понимание, сколько раз будут выполнены
        запросы.
        Для значительного увеличения производительности можно кешировать groups.

        Примечания к реализации:
            * Невозможно сгенерировать больше случайных записей, чем количество
              групп (Group). При необходимости можно доработать алгоритм.
            * Можно оптимизировать, переложив много запросов через ORM
              на один запрос, содержащий UNION.
        """
        st = time.time()
        groups = list(Group.objects.values_list('group', 'number'))
        # Кеширование групп
        # groups = self._groups
        parts = random.sample(range(0, len(groups) + 1), quantity + 1)
        offsets = []  # list of tuples(min_num, offset)
        for p_num in parts:
            p_offset = random.randrange(0, settings.PAGE_GROUP_SIZE)
            if p_num == 0:
                offsets.append((0, p_offset))
            else:
                offsets.append((groups[p_num - 1][1], p_offset))

        numbers = []
        for min_num, offset in offsets:
            # Запрашиваем только number, чтобы проверить существование.
            # НЕ извлекаем title, т.к. это "вешает БД" - сканируется таблица.
            rec = list(
                Record.objects.values_list('number', flat=True)
                .filter(number__gt=min_num)[offset:offset + 1]
            )
            if rec:
                numbers.append(rec[0])

        # Вариант а)
        # AVERAGE( 500 запусков ) = 0.122s
        # При кешировании групп есть небольшой прирост скорости:
        # AVERAGE( 500 запусков ) = 0.117s
        qs = Record.objects.values_list('number', 'title')\
            .filter(number__in=numbers[:quantity])
        recs = list(qs)

        # Вариант б)
        # AVERAGE( 500 запусков ) = 0.143s
        # recs = []
        # while len(recs) < quantity and numbers:
        #     n = numbers.pop(0)
        #     rec = Record.objects.values_list('number', 'title')\
        #         .get(number=n)[0]
        #     recs.append(rec)
        logger.info('Random generation time: %.3f' % (time.time() - st))
        return recs

    def get_next_numbers(self, quantity=3):
        next_pks = Record.objects.filter(number__gt=self.number)\
            .values_list('number', flat=True)[:quantity]
        next_pks = list(next_pks)

        # Необходимо получить number, затем по ним получить title.
        # Так работает быстрее, чем на вложенный запрос на уровне БД.
        next_recs = list(
            self.__class__.objects.filter(number__in=next_pks)\
            .values_list('number', 'title')
        )
        return next_recs


class Group(models.Model):
    A_DELETE = 'delete'
    A_INSERT = 'insert'

    group = models.IntegerField(primary_key=True)
    number = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'app_group%s' % db_table_suffix

    def __str__(self):
        return '%s: %s' % (self.group, self.number)

    @classmethod
    def _update_groups(cls, groups_qs, action):
        if action == cls.A_DELETE:
            filter_name = 'number__gt'
            order = 'number'
        else:
            filter_name = 'number__lt'
            order = '-number'

        cur_groups = list(
            groups_qs.order_by('number').values_list('group', 'number')
        )
        if cur_groups:
            new_groups = []

            if action == cls.A_DELETE:
                # В последней группе есть числа? Если нет, то её надо удалить
                last_recs = Record.objects.filter(
                    number__gt=cur_groups[-1][1]  # Число последней группы
                )
                if not last_recs.exists():
                    cur_groups = cur_groups[:-1]

            # Можно оптимизировать, переложив убрав запросы через ORM.
            # Сгенерировать SQL с UNION, ORDER BY number. Затем номера групп
            # подставить программно.
            for group, min_number in cur_groups:
                recs = list(
                    Record.objects.filter(**{filter_name: min_number})
                    .order_by(order).values_list('number', flat=True)[:1]
                )
                if recs:
                    new_groups.append(
                        cls(group=group, number=recs[0])
                    )
            return new_groups

    @classmethod
    def update_min_numbers(cls, number, action=A_DELETE):
        """Обновить минимальные значения чисел для групп при удалении/добавлении
        новых записей."""
        logger.info('Started')
        st = time.time()

        groups_qs = cls.objects.filter(number__gt=number)
        if action == cls.A_DELETE:
            new_groups = cls._update_groups(groups_qs, action)
        elif action == cls.A_INSERT:
            new_groups = cls._update_groups(groups_qs, action)
            if new_groups:
                # Количество групп увеличилось?
                last_group = new_groups[-1]
                last_group_num = last_group.number
                rec_qs = (
                    Record.objects.order_by('number')
                    .values_list('number', flat=True)
                    .filter(
                        number__gt=last_group_num
                    )[settings.PAGE_GROUP_SIZE - 1:settings.PAGE_GROUP_SIZE]
                )
                try:
                    new_group_num = rec_qs.get()  # NoQA
                except Record.DoesNotExist:
                    pass
                else:
                    new_groups.append(
                        Group(group=last_group.group + 1, number=new_group_num)
                    )
        else:
            raise ValueError('Incorrect action "%s"' % action)

        with transaction.atomic():
            groups_qs.delete()
            cls.objects.bulk_create(new_groups)

        logger.info('Finished in %.3f s' % (time.time() - st))
