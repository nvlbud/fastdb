# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


CREATE_TABLE = """
CREATE TABLE app_record_big (
    "number" INTEGER,
    title VARCHAR(30) NOT NULL,
    "text" TEXT
);
"""


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Record',
            fields=[
                ('number', models.IntegerField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=30)),
                ('text', models.TextField()),
            ],
            options={
                'managed': False,
                'ordering': ['number'],
            },
        ),

        migrations.RunSQL(CREATE_TABLE, reverse_sql=''),
    ]
