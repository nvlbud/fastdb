# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


CREATE_INDEX = """
CREATE UNIQUE INDEX app_record_big_number_idx ON app_record_big (number ASC);
"""

DROP_INDEX = "DROP INDEX app_record_number_idx;"


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(CREATE_INDEX, reverse_sql=DROP_INDEX),
    ]
