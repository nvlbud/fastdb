# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


CREATE_TABLE = """
CREATE TABLE app_group (
    "group" SMALLINT,
    "number" INTEGER
);
CREATE INDEX app_group_number_idx ON app_group (number ASC);
CREATE INDEX app_group_group_idx ON app_group ("group" ASC);
"""

class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app', '0002_create_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('group', models.IntegerField(primary_key=True, serialize=False)),
                ('number', models.IntegerField()),
            ],
            options={
                'managed': False,
            },
        ),

        migrations.RunSQL(CREATE_TABLE),
    ]
