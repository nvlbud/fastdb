from django.db import transaction, IntegrityError

from fastdb.app.models import Record, Group


def add_record(number, title, text):
    """Добавить новую запись с числом.
    Если уже существует, то обновлена не будет.
    """
    if Record.objects.filter(number=number).exists():
        return False

    with transaction.atomic():
        Record(number=number, title=title, text=text).save()
        Group.update_min_numbers(number, Group.A_INSERT)

    return True


def delete_record(number):
    """Удалить запись с указанным числом."""
    if not Record.objects.filter(number=number).exists():
        return False

    with transaction.atomic():
        Record.objects.filter(number=number).delete()
        Group.update_min_numbers(number, Group.A_DELETE)

    return True
