# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


CREATE_INDEX = """
CREATE UNIQUE INDEX app_record_01_number_idx ON app_record_01 (number ASC);
CREATE UNIQUE INDEX app_record_02_number_idx ON app_record_02 (number ASC);
CREATE UNIQUE INDEX app_record_03_number_idx ON app_record_03 (number ASC);
CREATE UNIQUE INDEX app_record_04_number_idx ON app_record_04 (number ASC);
CREATE UNIQUE INDEX app_record_05_number_idx ON app_record_05 (number ASC);
CREATE UNIQUE INDEX app_record_06_number_idx ON app_record_06 (number ASC);
CREATE UNIQUE INDEX app_record_07_number_idx ON app_record_07 (number ASC);
CREATE UNIQUE INDEX app_record_08_number_idx ON app_record_08 (number ASC);
CREATE UNIQUE INDEX app_record_09_number_idx ON app_record_09 (number ASC);
CREATE UNIQUE INDEX app_record_10_number_idx ON app_record_10 (number ASC);
"""


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(CREATE_INDEX, reverse_sql=''),
    ]
