# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


CREATE_TABLE = """
CREATE TABLE app_group_big (
    "group" SMALLINT,
    "number" INTEGER
);
CREATE INDEX app_group_big_number_idx ON app_group_big (number ASC);
CREATE INDEX app_group_big_group_idx ON app_group_big ("group" ASC);
"""


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app', '0002_create_index'),
    ]

    operations = [
        migrations.RunSQL(CREATE_TABLE),
    ]
