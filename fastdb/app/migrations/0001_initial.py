# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


CREATE_TABLE = """
CREATE TABLE app_record (
    "number" INTEGER,
    title VARCHAR(30) NOT NULL,
    "text" TEXT
);

CREATE TABLE app_record_01 (
    CHECK (number > 0 AND number <= 100000000)
) INHERITS (app_record);
CREATE TABLE app_record_02 (
    CHECK (number > 100000000 AND number <= 200000000)
) INHERITS (app_record);
CREATE TABLE app_record_03 (
    CHECK (number > 200000000 AND number <= 300000000)
) INHERITS (app_record);
CREATE TABLE app_record_04 (
    CHECK (number > 300000000 AND number <= 400000000)
) INHERITS (app_record);
CREATE TABLE app_record_05 (
    CHECK (number > 400000000 AND number <= 500000000)
) INHERITS (app_record);
CREATE TABLE app_record_06 (
    CHECK (number > 500000000 AND number <= 600000000)
) INHERITS (app_record);
CREATE TABLE app_record_07 (
    CHECK (number > 600000000 AND number <= 700000000)
) INHERITS (app_record);
CREATE TABLE app_record_08 (
    CHECK (number > 700000000 AND number <= 800000000)
) INHERITS (app_record);
CREATE TABLE app_record_09 (
    CHECK (number > 800000000 AND number <= 900000000)
) INHERITS (app_record);
CREATE TABLE app_record_10 (
    CHECK (number > 900000000 AND number <= 1000000000)
) INHERITS (app_record);


CREATE OR REPLACE FUNCTION number_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.number > 0 AND NEW.number <= 100000000) THEN
        INSERT INTO app_record_01 VALUES (NEW.*);
    ELSIF (NEW.number > 100000000 AND NEW.number <= 200000000) THEN
        INSERT INTO app_record_02 VALUES (NEW.*);
    ELSIF (NEW.number > 200000000 AND NEW.number <= 300000000) THEN
        INSERT INTO app_record_03 VALUES (NEW.*);
    ELSIF (NEW.number > 300000000 AND NEW.number <= 400000000) THEN
        INSERT INTO app_record_04 VALUES (NEW.*);
    ELSIF (NEW.number > 400000000 AND NEW.number <= 500000000) THEN
        INSERT INTO app_record_05 VALUES (NEW.*);
    ELSIF (NEW.number > 500000000 AND NEW.number <= 600000000) THEN
        INSERT INTO app_record_06 VALUES (NEW.*);
    ELSIF (NEW.number > 600000000 AND NEW.number <= 700000000) THEN
        INSERT INTO app_record_07 VALUES (NEW.*);
    ELSIF (NEW.number > 700000000 AND NEW.number <= 800000000) THEN
        INSERT INTO app_record_08 VALUES (NEW.*);
    ELSIF (NEW.number > 800000000 AND NEW.number <= 900000000) THEN
        INSERT INTO app_record_09 VALUES (NEW.*);
    ELSIF (NEW.number > 900000000 AND NEW.number <= 1000000000) THEN
        INSERT INTO app_record_10 VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION 'Number is out of range. Fix number_insert_trigger function.';
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_app_record_trigger
BEFORE INSERT ON app_record
FOR EACH ROW EXECUTE PROCEDURE number_insert_trigger();
"""


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Record',
            fields=[
                ('number', models.IntegerField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=30)),
                ('text', models.TextField()),
            ],
            options={
                'ordering': ['number'],
                'managed': False,
            },
        ),

        migrations.RunSQL(CREATE_TABLE, reverse_sql=''),
    ]
