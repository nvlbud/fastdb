import logging
import time
from functools import partial

from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, DetailView, CreateView

from fastdb.app.forms import RecordForm
from fastdb.app.lib import delete_record
from fastdb.app.models import Group, Record


logger = logging.getLogger('django')


def get_generation_time(start_time):
    return time.time() - start_time


class TimeMeasureMixin(object):
    start_time = 0

    def __init__(self, **kwargs):
        self.start_time = time.time()
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        logger.info('Get request {}'.format(request.get_full_path()))
        response = super().dispatch(request, *args, **kwargs)
        return response

    def render_to_response(self, context, **response_kwargs):
        context['generation_time'] = partial(get_generation_time,
                                             self.start_time)
        return super().render_to_response(context, **response_kwargs)


class RecordsListView(TimeMeasureMixin, TemplateView):
    template_name = 'records_list.html'
    model = Record
    RECS_PER_PAGE = 10

    def get_records(self, page):
        if page < 1:
            page = 1

        # Определяем номер группы
        real_offset = (page - 1) * self.RECS_PER_PAGE
        group_number = real_offset // settings.PAGE_GROUP_SIZE
        offset = real_offset % settings.PAGE_GROUP_SIZE

        records_qs = Record.objects.order_by('number')\
            .values_list('number', flat=True)

        if group_number > 0:
            # Найдено большое смещение, используем фильтр по индексу,
            # чтобы уменьшить OFFSET
            try:
                number = Group.objects.get(group=group_number).number
            except Group.DoesNotExist:
                # Не нашли номер группы => запрощено за пределами максимального
                # значения в БД
                return []

            records_qs = records_qs.filter(number__gt=number)

        numbers = list(records_qs[offset:offset + self.RECS_PER_PAGE])

        qs = Record.objects.filter(number__in=numbers).values('number', 'title')
        # logger.info('Query: %s' % qs.query)

        object_list = list(qs)
        return object_list

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        try:
            page = int(self.request.GET.get('page'))
        except (ValueError, TypeError):
            page = 1

        records = self.get_records(page)
        if records:
            # min_num = records[0]
            max_num = records[-1]['number']
            if page > 1:
                ctx['prev_page'] = page - 1
            next_rec = Record.objects.filter(number__gt=max_num)\
                .values_list('number')[:1]
            if next_rec:
                ctx['next_page'] = page + 1

        ctx['cur_page'] = page
        ctx['object_list'] = records
        return ctx


class RecordView(TimeMeasureMixin, DetailView):
    model = Record
    template_name = 'record.html'

    def get_object(self, queryset=None):
        logger.info('Fetching record')
        rec = Record.objects.get(number=self.kwargs.get('pk'))
        logger.info('Record fetched')
        return rec

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        if self.object:
            logger.info('Fetch random')
            ctx['random_recs'] = Record.get_random_numbers()
            logger.info('Random fetched')
            logger.info('Fetch next')
            ctx['next_recs'] = self.object.get_next_numbers()
            logger.info('Next fetched')

        return ctx


def delete_record_view(request, pk):
    """Удалить запись и перенаправить на ранее открытую страницу"""
    start_time = time.time()
    delete_record(number=pk)
    logger.info(
        'Record "%s" deleted in %.3f s' % (pk, time.time() - start_time)
    )
    return HttpResponseRedirect(
        '%s?page=%s' % (reverse('records_list'), request.GET.get('page', ''))
    )


class AddRecordView(TimeMeasureMixin, CreateView):
    template_name = 'record_add.html'
    form_class = RecordForm
    success_url = reverse_lazy('records_list')

    def form_valid(self, form):
        logger.info('Record created in %.3f s' % (time.time() - self.start_time))
        return super().form_valid(form)
