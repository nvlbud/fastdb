from django import forms

from fastdb.app.lib import add_record
from fastdb.app.models import Record


class RecordForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = ['number', 'title', 'text']

    def save(self, commit=True):
        add_record(**self.cleaned_data)
        # Для поддержки стандартного view
        return Record(number=self.cleaned_data['number'])
