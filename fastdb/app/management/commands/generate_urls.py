import logging
import os
import random
import time

from django.conf import settings
from django.core.management.base import BaseCommand

from fastdb.app.models import Record, RANDOM_CHOICES


URLS_FILE = os.path.join(settings.BASE_DIR, 'tmp/urls.txt')


class Command(BaseCommand):
    help = 'Сгенерировать ссылки на случайные страницы'

    def add_arguments(self, parser):
        parser.add_argument(
            '--total', metavar='total', default=10 ** 4, type=int,
            help='Количество ссылок'
        )
        parser.add_argument(
            '--max-page', metavar='max_page', default=5 * 10 ** 6, type=int,
            help='Максимальный номер страницы'
        )
        parser.add_argument(
            '--site-url', metavar='site_url', default='http://localhost:8000',
            type=str, help='URL сайта (без слеша на конце)'
        )
        parser.add_argument(
            '--records', action='store_true', default=False,
            help='Ссылки на страницы записей'
        )

    def handle(self, *args, **options):
        start_time = time.time()
        logger = logging.getLogger('django')
        random.seed()

        total = options.get('total')
        max_page = options.get('max_page')
        site_url = options.get('site_url')

        f = open(URLS_FILE, 'w')

        if options.get('records'):
            numbers = set()
            while len(numbers) < total:
                recs = Record.objects.filter(
                    number__in=set(random.sample(RANDOM_CHOICES, 500))
                ).values_list('number', flat=True)
                numbers.update(set(recs))
                logger.info('Done: {}'.format(len(numbers)))

            for n in numbers:
                f.write('{}/{}\n'.format(site_url, n))
        else:
            part_size = max_page // total
            for x in range(total):
                page = random.randint(x * part_size, (x + 1) * part_size)
                f.write('{}/?page={}\n'.format(site_url, page))

        f.close()

        logger.info('Completed in {} s'.format(time.time() - start_time))
