import logging
import time

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction

from fastdb.app.models import Record, Group


class Command(BaseCommand):
    help = 'Сгенерировать группы по страницам'

    def handle(self, *args, **options):
        start_time = time.time()
        logger = logging.getLogger('django')

        logger.info('Creating groups '.format(time.time()))

        ordered_nums = Record.objects.order_by('number')\
            .values_list('number', flat=True)

        groups_numbers = []
        min_num = 0  # По задаче используются от 1 до 1 млрд
        iteration = 0

        offset = settings.PAGE_GROUP_SIZE
        while min_num is not None:
            iter_start = time.time()
            rec_qs = ordered_nums.filter(number__gt=min_num)[offset - 1:offset]
            logger.debug(str(rec_qs.query))

            try:
                cur_number = rec_qs.get()  # NoQA
            except Record.DoesNotExist:
                min_num = None
            else:
                min_num = cur_number
                groups_numbers.append(cur_number)
                logger.info('cur_number: %s' % cur_number)
                iteration += 1

            iter_time = time.time() - iter_start
            logger.info(
                'Iteration {}: {}'.format(iteration, iter_time)
            )

            if iter_time > 40.0:
                logger.error('Too much time per iteration. Exiting')
                return

        with transaction.atomic():
            Group.objects.all().delete()

            Group.objects.bulk_create(
                [
                    Group(group=i, number=num)
                    for i, num in enumerate(groups_numbers, 1)
                ]
            )

        logger.info('Completed in {} s'.format(time.time() - start_time))
