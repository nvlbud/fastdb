import functools
import logging
import time

from django.core.management.base import BaseCommand

from fastdb.app.models import Record, Group


class Command(BaseCommand):
    help = 'Проверить количество результатов генерации случайных чисел'

    def add_arguments(self, parser):
        parser.add_argument(
            '--total', metavar='max_value', default=10 ** 4, type=int,
            help='Количество проверок'
        )
        parser.add_argument(
            '--quantity', metavar='quantity', default=3, type=int,
            help='Количество случайных чисел'
        )
        parser.add_argument(
            '--progress', metavar='progress', default=20, type=int,
            help='N раз вывести информационное сообщение с процентом выполнения'
        )
        parser.add_argument(
            '--fast', default=True, action='store_true',
            help='Проверить "get_random_numbers_fast"'
        )

    def handle(self, *args, **options):
        start_time = time.time()
        logger = logging.getLogger('django')

        logger.info('Start checking '.format(time.time()))

        total = options['total']
        quantity = options['quantity']
        progress = options['progress']
        steps = total // progress

        if options.get('fast'):
            rand_getter = Record().get_random_numbers_fast
        else:
            rand_getter = functools.partial(
                Record.get_random_numbers, fast=True
            )

        errors = 0
        for i in range(total):
            if not i % steps:
                logger.info('Iteration {}'.format(i))

            qtn = len(rand_getter(quantity))
            if qtn < quantity:
                logger.error(
                    'Quantity is less then required: {} of {}'.format(
                        qtn, quantity
                    )
                )
                errors += 1

        logger.info('Completed in {} s'.format(time.time() - start_time))
        logger.info('Errors: {}'.format(errors))
