import logging
import os
import psutil
import random
import time

from django.conf import settings
from django.core.management.base import BaseCommand

from fastdb.app.models import Record


WORDS_FILE = os.path.join(settings.BASE_DIR, 'tmp/words.txt')
TITLE_WORDS = 3
TEXT_WORDS = 30

DB_FILES = 10


def load_dictionary():
    """Загрузка словаря"""
    if not os.path.exists(WORDS_FILE):
        return None

    f = open(WORDS_FILE, 'r')
    words = list(filter(None, map(str.strip, f.readlines())))
    return words


def generate_string(dictionary, quantity):
    """Сгенерировать случайную строку из quantity слов"""
    return ' '.join([random.choice(dictionary) for _ in range(quantity)])


def generate_strings(dictionary, quantity, qtn_title, qtn_text, title_length):
    """Сгенерировать случайную строку из quantity слов"""
    random.shuffle(dictionary)
    pos = 0
    max_pos = len(dictionary) - qtn_title - qtn_text
    for _ in range(quantity):
        if pos >= max_pos:
            # Начать с начала, немного случайности
            pos %= 10

        prev_pos = pos
        pos += qtn_title
        title = ' '.join(dictionary[prev_pos:pos])

        prev_pos = pos
        pos += qtn_text
        text = ' '.join(dictionary[prev_pos:pos])
        yield title[:title_length], text


def generate_numbers(start, finish, quantity) -> 'list of int':
    """
    Сгенерировать quantity чисел из диапазона [start, finish].
    Только четные числа.
    """
    all_numbers = list(range(start, finish, 2))
    numbers = random.sample(all_numbers, quantity)
    return numbers


class Command(BaseCommand):
    help = 'Заполнить БД случайными записями'

    def add_arguments(self, parser):
        parser.add_argument(
            '--max-value', metavar='max_value', default=10 ** 9, type=int,
            help='Максимальное значение номера'
        )
        parser.add_argument(
            '--bulk-size', metavar='bulk_size', default=10 ** 6, type=int,
            help='Количество от общего, обрабатываемое за итерацию'
        )
        parser.add_argument(
            '--total', metavar='total', default=50 * 10 ** 6, type=int,
            help='Количество случайных записей'
        )

    def handle(self, *args, **options):
        start_time = time.time()
        logger = logging.getLogger('django')
        random.seed()
        process = psutil.Process(os.getpid())

        max_title_length = Record._meta.get_field('title').max_length

        total = options.get('total')
        max_value = options.get('max_value')
        bulk_size = options.get('bulk_size')

        iterations = max_value // bulk_size
        sample_size = total // iterations  # Генерируемые значения за один шаг

        logger.info(
            'Options:\n'
            '\tTotal: {}\n'
            '\tMax value: {}\n'
            '\tBulk size: {}\n'
            '\tIterations: {}\n'
            '\tIteration sample: {}'.format(
                total, max_value, bulk_size, iterations, sample_size
            )
        )

        logger.info(
            'Loading dictionary.'
            'Memory usage: {}'.format(process.memory_info_ex())
        )
        dictionary = load_dictionary()
        if not dictionary:
            logger.error('Can not load dictionary')
            return

        file_records = iterations // 10
        for k in range(DB_FILES):
            out_file = open(
                os.path.join(settings.BASE_DIR, 'tmp/db-{}.csv'.format(k)),
                'w'
            )

            for i in range(k * file_records, (k + 1) * file_records):
                i % 50 == 0 and logger.info(
                    'Iteration: {}, '
                    'Memory usage: {}'.format(i, process.memory_info_ex())
                )
                numbers = generate_numbers(
                    i * bulk_size + 2,  # левая граница не входит
                    (i + 1) * bulk_size,
                    sample_size
                )

                i % 50 == 0 and logger.info('Generating records')
                strings = generate_strings(
                    dictionary, sample_size,
                    TITLE_WORDS, TEXT_WORDS, max_title_length
                )
                for j, values in enumerate(strings):
                    out_file.write('{},{},{}\n'.format(numbers[j], *values))

            out_file.close()

        logger.info('Completed in {} s'.format(time.time() - start_time))
