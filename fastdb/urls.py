from django.conf.urls import url

from fastdb.app.views import RecordsListView, RecordView, delete_record_view, \
    AddRecordView


urlpatterns = [
    url(r'^$', RecordsListView.as_view(), name='records_list'),
    url(r'^add$', AddRecordView.as_view(), name='record_add'),
    url(r'^(?P<pk>\d+)$', RecordView.as_view(), name='record_view'),
    url(r'^(?P<pk>\d+)/delete$', delete_record_view, name='record_delete'),
]
